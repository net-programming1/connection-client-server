import socket

def client_program():
    host = '127.0.0.1'
    port = 47749

    client = socket.socket()
    client.connect((host, port))

    num = 1
    while num <= 100:
        client.send(str(num).encode())
        data = client.recv(1024).decode()
        print("Received from server: "+data)
        num = int(data) + 1
    client.close()

if __name__ == '__main__':
    client_program()